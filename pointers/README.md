## Pointers
 > Declaring and Assigning

  * Declaring a pointer
  ``` go
  var p *int
  ```
  * Replace  p with pointer name and int with the type of data you want it point at
  * Assigning a pointer
  ``` go
  p = &v
  ```
  * Change v with whatever variable you want it point at make sure the variable matches the data type
  * Make sure to have & before variable name so it shows you want it to point at that variable
  ``` go
  pointer1 := &value1
  ```
  * Declaring and assigning implicitly
