## Variables
> Explicitly typed

 * First put ```var``` keyword
 * Second put variable name
 * Third put assignment operator ```=```
 * Last put data  

> Implicitly typed   

  * Use ```:=``` as assignment operator and you dont have to put
  * Both are still static and cant be changed
## Constants
* A constant is a simple, unchanging value

> Explicitly typed  

  * Same as variables

> Implicitly typed

  * Same as variables just remove ```:```
## Types
* All lowercase single words

> Bool  

* ```bool```

> String  

  * ```string```

> Integer Types  

* The ```u``` stands for unsigned
* The number after changes how high of a number it can hold
* Unfixed or aliases just depend on your system to define its bytes
* Fixed:
  * ```uint8```
  * ```uint16```
  * ```uint32```
  * ```uint64```
  * ```int8```
  * ```int16```
  * ```int32```
  * ```int64```
* Unfixed:
  * ```byte```
  * ```uint```
  * ```int```
  * ```uintptr``
* Floats:
  * ```float32```
  * ```float64````
* Complex numbers:
  * ```complex64```
  * ```complex128```
* Data collections:
  * ```arrays```
  * ```slices```
  * ```maps```
  * ```structs```
* Language organization:
  * ```functions```
  * ```interfaces```
  * ```strings```
* Complex Types
  * ```pointer```
