package main

import (
	"github.com/skilstak/go-input"
	"fmt"
	"regexp"
)

func main() {
	exp := input.Ask("Regular Expression? ")
	r, err := regexp.Compile(exp)
	if err != nil {
		fmt.Println("Your code is bad")
		fmt.Print(err)
		main()
	}
	action := input.Ask("Which action (match, find)? ")
	switch action {
	case "match":
		fmt.Println(r.MatchString(input.Ask("string? ")))
	case "find":
		fmt.Println(r.FindString(input.Ask("string? ")))
	}
}