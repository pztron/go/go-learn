package main

import (
	"github.com/skilstak/go-input"
	"fmt"
	"regexp"
	"os"
)

func main() {
	option := os.Args[1]
	switch option {
	case "-test":
		test()
	case "-create":
		custom()
	} /* else {
		fmt.Println("The options are: -)
	} */
}

func custom() {
	exp := input.Ask("Regular Expression? ")
	r, err := regexp.Compile(exp)
	if err != nil {
		fmt.Println("Your code is bad")
		fmt.Print(err)
		custom()
	}
	action := input.Ask("Which action (match, find)? ")
	switch action {
	case "match":
		fmt.Println(r.MatchString(input.Ask("string? ")))
	case "find":
		fmt.Println(r.FindString(input.Ask("string? ")))
	}
}
func test() {
	fmt.Println("something here")
}