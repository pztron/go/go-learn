package main

import (
	"fmt"
	"time"

	c "github.com/skilstak/go-colors"
)

func main() {

	for {
		// sleep one second
		time.Sleep(100 * time.Millisecond)
		// clear screen
		fmt.Println(c.CL + "")
		// print time
		fmt.Printf(c.Yellow+"%s\n", time.Now()-12)

	}
}
