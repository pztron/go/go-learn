package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(repeatstr(5, "hello"))
}
func repeatstr(n int, s string) string {
	return strings.Repeat(s, n)
}
